import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_image(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": city + " " + state}
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_coordinates(city, state):
    # res = requests.get(f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}')
    # data = json.loads(res.text)
    # return {
    #     "lat":data[0]["lat"],
    #     "lon":data[0]["lon"]
    # }
    params = {"q": f"{city},{state},USA",
              "limit": "1",
              "appid": OPEN_WEATHER_API_KEY}
    url = "http://api.openweathermap.org/geo/1.0/direct?"

    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        return {"lat":content[0]["lat"],
                "lon":content[0]["lon"]
    }
    except:
        return {"lat": None, "lon": None}


def get_weather_data(latitude, longitude):
    # res = requests.get(f'https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&units=imperial&appid={OPEN_WEATHER_API_KEY}')
    # data = json.loads(res.text)
    # return {
    #     "temp":data["main"]["temp"],
    #     "description": data["weather"][0]["description"]
    # }
    params = {"lat": latitude,
              "lon": longitude,
              "units": "imperial",
              "appid": OPEN_WEATHER_API_KEY}
    url = "https://api.openweathermap.org/data/2.5/weather?"

    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
        "temp":content["main"]["temp"],
        "description": content["weather"][0]["description"]
    }
    except:
        return {"temp": None,
                "description": None}
